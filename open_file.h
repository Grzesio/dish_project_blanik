#include <iostream>
#include <string>

using namespace std;

#ifndef OPEN_FILE_H_INCLUDED
#define OPEN_FILE_H_INCLUDED



#endif // OPEN_FILE_H_INCLUDED

class open_file {
    string name_of_file;
    int number_of_elements;
    string disp_buffor;
    public:
        string what_contain[16];
        open_file(string, int);
        void disp_everything();
        friend class energy_convertion;
        friend class choosing_program;
//        friend void run_program(open_file & my_file, real_time & start_time, real_time & end_time, energy_convertion & Energy, time_convertion & Duration, choosing_program & program_selection, rotation & r1);
};
open_file::open_file (string a, int b){
    number_of_elements = b;
    name_of_file = a;
    string disp_program[b];
    fstream file;
    file.open( a, ios::in | ios::out );
    if( file.good() == true ) {
        cout << "File access granted!" << endl;
        for (int i = 0; i < b; i++){
            getline(file, disp_buffor);
            what_contain[i] = disp_buffor;
        }
        file.close();
        }
    else cout << "File access forbidden " << endl;
}
void open_file::disp_everything(){
    cout << what_contain[0] << "\t\t" << "temp: " << what_contain[8] << "\t";
    cout << "time: " << what_contain[4] << " sec" << "\tEnergy: " << what_contain[12] << " kW" << "\n";
    cout << what_contain[1] << "\t" << "temp: " << what_contain[9] << "\t";
    cout << "time: " << what_contain[5] << " sec" << "\tEnergy: " << what_contain[13] << " kW" << "\n";
    cout << what_contain[2] << "\t\t\t" << "temp: " << what_contain[10] << "\t";
    cout << "time: " << what_contain[6] << " sec" << "\tEnergy: " << what_contain[14] << " kW" << "\n";
    cout << what_contain[3] << "\t" << "temp: " << what_contain[11] << "\t";
    cout << "time: " << what_contain[7] << " sec" << "\tEnergy: " << what_contain[15] << " kW" << "\n";
}
