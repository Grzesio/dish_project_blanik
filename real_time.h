#include <iostream>

#ifndef REAL_TIME_H_INCLUDED
#define REAL_TIME_H_INCLUDED



#endif // REAL_TIME_H_INCLUDED

class real_time{
    public:
        time_t currently_time;
        int difference;
        int pause_buffor = 0;
        void set_time();
        void difference_in_time(long, long);
        void pause_module(long begining, long ending);
//        friend void run_program(open_file & my_file, real_time & start_time, real_time & end_time, energy_convertion & Energy, time_convertion & Duration, choosing_program & program_selection, rotation & r1);
};
void real_time::set_time(){
    time( & currently_time );
}
void real_time::difference_in_time(long begining, long ending){
    difference = difftime( ending, begining );
}
void real_time::pause_module(long begining, long ending){
    pause_buffor = difftime( ending, begining );
}
