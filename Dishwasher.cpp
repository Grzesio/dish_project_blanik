#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <time.h>
#include <windows.h>

#include "open_file.h"
#include "choosing_program.h"
#include "time_convertion.h"
#include "energy_convertion.h"
#include "real_time.h"
#include "rotation.h"
using namespace std;

void run_program(open_file & my_file, real_time & start_time, real_time & end_time, energy_convertion & Energy, time_convertion & Duration, choosing_program & program_selection, rotation & r1){
    int abort_variable = 0, abort_release_buffer = 0;
    float energy_consumption = 0;
    while(end_time.difference < Duration.converted_time && abort_variable != 1){
        if (GetKeyState(0x50) < 0)  {
            end_time.pause_module(start_time.currently_time, end_time.currently_time);
                cout << "\n\n\t\t\t      PAUSE" << endl << "\t\t\tPress \"p\" to resume" << endl;
                for(int i=5; i>4;i++){
                    if (GetKeyState(0x50) < 0){
                    }else{
                        i=1;
                    }
                }
                for(int i=5; i>4;i++){
                    if (GetKeyState(0x50) < 0)  {
                        i=1;
                    }
                    if (GetKeyState(0x41) < 0) {
                        i=1;
                        abort_variable = 1;
                    }
                }
                for(int i=5; i>4;i++){
                    if (GetKeyState(0x50) < 0 || GetKeyState(0x41)){
                    }else{
                        i=1;
                    }
                }
                start_time.set_time();
                start_time.currently_time = start_time.currently_time - end_time.pause_buffor;
        }
        if (GetKeyState(0x41) < 0) {
            abort_release_buffer = 1;
            abort_variable = 1;
        }
        if (abort_release_buffer == 1){
            for(int i=5; i>4;i++){
                if (GetKeyState(0x41) < 0) {
                }else{
                    i=1;
                }
            }
        }
        end_time.set_time();
        end_time.difference_in_time(start_time.currently_time, end_time.currently_time);
        end_time.difference = Duration.converted_time - end_time.difference;
        energy_consumption = Energy.converted_energy * (end_time.difference / Duration.converted_time);
        system ("cls");
        cout << my_file.what_contain[program_selection.selected_program - 1] << "\t" << "temp: " << my_file.what_contain[program_selection.selected_program + 7] << "\t";
        cout << "time: " << end_time.difference << "sec left" << "\tEnergy: " << energy_consumption << " kW left" << "\n\n\n";
        r1.rotation_motion();
        Sleep(20);
        r1.rotations = r1.rotations +1;
        end_time.difference_in_time(start_time.currently_time, end_time.currently_time);
    }
}

int main(){
    string a = "file123.txt";
    open_file my_file(a, 16);
    choosing_program program_selection;
    energy_convertion Energy;
    time_convertion Duration;
    real_time start_time;
    real_time end_time;
    rotation r1;
    for (int i = 1; i <6; i++){
    program_selection.programs(my_file);
    Energy.convertion(my_file, program_selection);
    Duration.convertion(my_file, program_selection);
    start_time.set_time();
    end_time.set_time();
    end_time.difference_in_time(start_time.currently_time, end_time.currently_time);
    run_program(my_file, start_time, end_time, Energy, Duration, program_selection, r1);
    system ("cls");
    }
    return 0;
}
